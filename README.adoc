= Brloz

Pronounced "Berlioz," as in a reference to Hector Berlioz, this is a very
demanding conductor of tests. Brloz orchestrates a pipeline of tests
using a very simple interface to a cloud provider.

Initially only Kubernetes is supported, but the intent is for many different
types of testing pipelines to be supported.

== Motivation

Testing microservices is hard. There are far more integration planes
with microservices. We would benefit from a method to deploy and integrate
code _not_ in a production configuration, but in an intentionally testing 
configuration.

Testing the final deployed artifact biases developers to expose internal logic
to an external system. This can be a vulnerability. Better to have a simple
alternative means to run the production code, but in a testing configuration.

An example: consider the semnatics of queing system, such as SQS. You could mock
SQS. You could wire up the final production artifact to have a testing mode, but
this may put production at risk or include unecessary telemetry that bloats your
logs. Better to deploy some code into the cluster that can leverage a testing SQS
queue.

