
import os
import yaml
import traceback

from .. import JobRegistry
from .. import CollectorRegistry
from .. import ReporterRegistry
from .. import Stage

def reportersFromDict(d):
    '''An internal function to convert a Python dict to a Reporter.'''
    return ReporterRegistry.reporter(d['type'], **d.get('args', {}))

def stageFromDict(d):
    '''An internal function to convert a Python dict to a Stage.'''

    if 'collector' in d:
        c = d['collector']
        collector = CollectorRegistry.collector(c['type'], **c.get('args', {}))
    else:
        collector = CollectorRegistry.collector('all')

    return Stage(
        d['name'],
        d.get('timeout', 10),
        JobRegistry.job(d['job']['type'], **d['job'].get('args', {})),
        collector,
        [ reportersFromDict(r) for r in d.get('reporters', [])])

class Pipeline:
    def __init__(self, name, stages=[]):
        self.name = name
        self.stages = stages
    
    def run(self):
        ''' Call run() on each stage in the order it was added.

        Then call cleanup() on each stage in the reverse order it was added.

        Raised Exceptions are printed, but not other action is taken.
        '''
        for stage in self.stages:
            try:
                stage.run()
            except Exception as e:
                print("Exception in stage "+stage.name)
                # print(e.status)
                # print(e.reason)
                # print(e.http_resp)
                print(e)
                traceback.print_tb(e.__traceback__)
        
        # NOTE: Cleanup happens in reverse order.
        for stage in reversed(self.stages):
            try:
                stage.cleanup()
            except Exception as e:
                print("Exception in cleanup of stage "+stage.name)
                traceback.print_tb(e.__traceback__)
    
    def withStage(self, stage):
        '''Append stage to the list of stages to execute and return self.

        This allows for a fluent style of chaining series of withStage() calls.
        '''
        self.stages.append(stage)
        return self
    
    @staticmethod
    def fromYaml(stream):
        '''Load from the stream and consruct a pipeline.
        '''
        r = yaml.load(stream, Loader=yaml.Loader)

        pipeline = Pipeline(r['name'], [ stageFromDict(d) for d in r['stages']])

        return pipeline
            

class PipelineReporter:
    '''A class that collects success and failure results for a pipeline.
    This may be queried after a pipeline runs for produce reports.
    '''
    def __init__(self, **kwargs):
        self.successCount = 0
        self.failureCount = 0
        self.stageLog = []
    
    def report(self, name, status, results):
        if status.succeeded:
            if results.succeeded:
                self.successCount += 1
                self.stageLog.append({'stage': name, 'status': 'passed', 'when': 'results'})
            else:
                self.failureCount += 1
                self.stageLog.append({'stage': name, 'status': 'failed', 'when': 'results'})
        else:
            self.failureCount += 1
            self.stageLog.append({'stage': name, 'status': 'failed', 'when': 'job'})

ReporterRegistry.register('pipeline', PipelineReporter)