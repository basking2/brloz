import configparser
import os

def load_config(*files):
    config = configparser.ConfigParser()

    config.read([
                os.path.join(os.environ['HOME'], '.brlozrc'),
                '/etc/brlozrc',
                *files,
                ])
                
    return config

