
class Result:
    def __init__(self):
        self.succeeded = None
        self.failed = None
        self.message = None
        self.raw = None

class CollectorRegistryImpl:
    def __init__(self):
        self.collectors = {}
    
    def register(self, name, collector):
        self.collectors[name] = collector
    
    def collector(self, name, **kwargs):
        return self.collectors[name](**kwargs)

CollectorRegistry = CollectorRegistryImpl()

from .junit import JunitResultCollector
from .regex import RegexResultCollector
from .locust import LocustResultCollector
from .all import AllResultCollector