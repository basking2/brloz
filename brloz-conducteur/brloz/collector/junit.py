
from . import Result

import xml.sax

class ContentHandler(xml.sax.ContentHandler):
    def __init__(self, result):
        xml.sax.ContentHandler.__init__(self)
        self.result = result
    
    def startDocument(self):
        self.result.succeeded = None
        self.result.message = "Parsing..."

    def startElement(self, name, attrs):
        if name == 'testsuite':
            errors = int(attrs.get('errors', '0'))
            failures = int(attrs.get('failures', '0'))
            skipped = int(attrs.get('skipped', '0'))
            tests = int(attrs.get('tests', '0'))

            if errors > 0 or failures > 0:
                self.result.succeeded = False
                self.result.failed = True
                self.result.message = "Failed because of {} errors and {} failures from {} tests. {} skipped.".format(errors, failures, tests, skipped)
            else:
                self.result.succeeded = True
                self.result.failed = False
                self.result.message = "Passed because of {} errors and {} failures from {} tests. {} skipped.".format(errors, failures, tests, skipped)

class JunitResultCollector:
    def __init__(self, **kwargs):
        self.result = Result()
    
    def collect(self, data):
        handler = ContentHandler(self.result)
        xml.sax.parseString(data, handler)
        self.result.raw = data
        return self.result

from . import CollectorRegistry

CollectorRegistry.register('junit', JunitResultCollector)

