
import re
from . import Result

class RegexResultCollector:
    def __init__(self, **kwargs):
        pattern = kwargs.get('regex', '.*')
        if type(pattern) == str:
            pattern = re.compile(pattern)
        self.re = pattern

    def collect(self, data):
        m = self.re.search(data)
        result = Result()
        if m:
            result.succeeded = True
            result.failed = False
            result.message = "Matched "+m[0]
            result.raw = data
        else:
            result.succeeded = False
            result.failed = True
            result.message = "No match found."
            result.raw = data
        return result

from . import CollectorRegistry

CollectorRegistry.register('regex', RegexResultCollector)

