
import re
from . import Result
import brloz.util.hashmatch

class LocustResultCollector:

    statre = re.compile('\\s*(\\w+ [^\\s]+|Aggregated)\\s+(\\d+)\\s+(\\d)\\((\\d+\\.\\d+)%\\)\\s*\\|\\s*(\\d+)\\s+(\\d+)\\s+(\\d+)\\s+(\\d+)\\s*\\|\\s*(\\d+.\\d+)\\s+(\\d+.\\d+)')

    logline = re.compile('^\\[.*\\]')

    def __init__(self, **kwargs):
        self.checks = []
        for check in kwargs.get('checks', []):
            self.checks.append({ 
                'if': [ brloz.util.hashmatch.build(l) for l in check.get('if', []) ],
                'then': [ brloz.util.hashmatch.build(l) for l in check.get('then', []) ],
                'name': check.get('message', "Failed check."),
                })
    
    def collect(self, data):

        for line in data.split("\n"):
            if LocustResultCollector.logline.search(line):
                continue

            #  Name                                                          # reqs      # fails  |     Avg     Min     Max  Median  |   req/s failures/s
            # --------------------------------------------------------------------------------------------------------------------------------------------
            #  GET /a/cF[x=13]                         1     0(0.00%)  |      11      11      11      11  |    0.00    0.00
            #  GET /c/wT[x=9]                              4     0(0.00%)  |      20      18      22      19  |    0.01    0.00
            # --------------------------------------------------------------------------------------------------------------------------------------------
            #  Aggregated                                                    249591     0(0.00%)  |      11       1     883       9  |  831.97    0.00
            m = LocustResultCollector.statre.search(line)
            if m:
                m = {
                    'id': m[1],
                    'type': 'statistics',
                    'requests': int(m[2]),
                    'failures': float(m[3]),
                    'failure_percentage': float(m[4]),
                    'average': int(m[5]),
                    'minimum': int(m[6]),
                    'maximum': int(m[7]),
                    'median': int(m[8]),
                    'requests_per_second': float(m[9]),
                    'failures_per_second': float(m[10]),
                    }
                for c in self.checks:
                    b = True
                    for given in c['if']:
                        b = b and given(m)
                    if b:
                        b = True
                        for check in c['then']:
                            b = b and check(m)
                        if not b:
                            result = Result()
                            result.succeeded = False
                            result.failed = True
                            result.message = "Failure for {} on record {}".format(c['name'], line)
                            result.raw = line
                            return result

            #  Type     Name                                                              50%    66%    75%    80%    90%    95%    98%    99%  99.9% 99.99%   100% # reqs
            # --------|------------------------------------------------------------|---------|------|------|------|------|------|------|------|------|------|------|------|
            # GET      /a/fl[x=13]                               8      8     10     10     10     10     10     10     10     10     10      6
            #  GET      /c/wT[x=9]                                  22     22     22     22     22     22     22     22     22     22     22      4
            # --------|------------------------------------------------------------|---------|------|------|------|------|------|------|------|------|------|------|------|
            #  None     Aggregated                                                          9     12     14     15     21     27     34     42     99    330    880 249591
            r = re.compile('\\s*(\\w+)\\s+([^\\s]+)\\s+' +
                    '(\\d+)\\s+' + # 50%
                    '(\\d+)\\s+' + # 66%
                    '(\\d+)\\s+' + # 75%
                    '(\\d+)\\s+' + # 80%
                    '(\\d+)\\s+' + # 90%
                    '(\\d+)\\s+' + # 95%
                    '(\\d+)\\s+' + # 98%
                    '(\\d+)\\s+' + # 99%
                    '(\\d+)\\s+' + # 99.9%
                    '(\\d+)\\s+' + # 99.99%
                    '(\\d+)\\s+' + # 100%
                    '(\\d+)' # reqs
                    )
            m = r.match(line)
            #if m:
                #print(m[0])

        result = Result()
        result.succeeded = True
        result.failed = False
        result.message = "No failures."
        result.raw = data
        return result

from . import CollectorRegistry

CollectorRegistry.register('locust', LocustResultCollector)

