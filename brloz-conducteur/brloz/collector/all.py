
from . import Result

# A very simple collector that always succeeds in collecting all output.
class AllResultCollector:
    def __init__(self, **kwargs):
        pass

    def collect(self, data):
        result = Result()
        result.succeeded = True
        result.failed = False
        result.message = "Success!"
        result.raw = data
        return result

from . import CollectorRegistry

CollectorRegistry.register('all', AllResultCollector)

