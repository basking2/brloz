import time
import datetime

class JobRegistryImpl:
    def __init__(self):
        self.registry = {}

    def register(self, name, clazz):
        self.registry[name] = clazz
    
    def job(self, jobType, **kwargs):
        return self.registry[jobType](**kwargs)

JobRegistry = JobRegistryImpl()

class JobBase:
    def __init__(self, **kwargs):
        self.completedState = None
        pass

    def status(self):
        '''
        Returns a object with succeeded or failed members defined.
        '''
        raise Exception("Status method is not implemented.")

    def run(self, **kwargs):
        raise Exception("Run method is not implemented.")

    def cleanup(self):
        raise Exception("Cleanup method is not implemented.")

    def logs(self):
        raise Exception("Logs method is not implemented.")

    def wait(self, timeout=None):
        '''
        timeout - seconds to wait.

        Returns the status object of the job implementation. None otherwise.
        '''
        startTime = datetime.datetime.now()
        while not self.completedState:
            status = self.status()
            if status.succeeded:
                self.completedState = status
            elif status.failed:
                self.completedState = status
            else:
                time.sleep(1)
                if timeout:
                    currentTime = datetime.datetime.now()
                    timeDelta = (currentTime - startTime).total_seconds()
                    # print(timeDelta, timeout)
                    if timeDelta > timeout:
                        print("Job timed out.")
                        return None
        return self.completedState

from .kubernetes import KubernetesJob
from .local import LocalJob
