
from . import JobBase, JobRegistry

from subprocess import Popen, PIPE
from io import BytesIO

class LocalJob(JobBase):
    def __init__(self, **kwargs):
        JobBase.__init__(self, **kwargs)
        self.succeeded = None
        self.failed = None
        self.logdata = None
        command = kwargs.get('command', [])
        if type(command) == str:
            command = [command]
        args = kwargs.get('args', [])
        if type(args) == str:
            args = [args]
        command = command + args

        self.command = command

    def status(self):
        if not self.succeeded and not self.failed:
            rc = self.proc.poll()
            if rc == 0:
                stdout, stderr = self.proc.communicate()
                self.logdata = stdout + stderr
                self.succeeded = True
            elif rc:
                stdout, stderr = self.proc.communicate()
                self.logdata = stdout + stderr
                self.failed = rc
        return self

    def run(self, **kwargs):
        try:
            self.proc = Popen(self.command, text=True, stdout=PIPE, stderr=PIPE)
        except Exception as exc:
            self.logdata = '{}: {}'.format(type(exc).__name__, exc)
            self.failed = '{}: {}'.format(type(exc).__name__, exc)

    def cleanup(self):
        pass

    def logs(self):
        return self.logdata

JobRegistry.register('local', LocalJob)