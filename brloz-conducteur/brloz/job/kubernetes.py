from . import JobRegistry
from . import JobBase

from kubernetes import client, config, utils
import uuid
import time

def _load_config():
  try:
    config.load_kube_config()
  except:
    config.load_incluster_config()


def _merge(dst, src):
    '''
    Merge the source hash into the destination hash.

    Keys that exist src are set in dst. Keys that are hashes in both are recursively merged.
    '''
    for k, v in src.items():
        if isinstance(v, dict):
            v2 = dst.setdefault(k, {})
            _merge(v2, v)
        elif isinstance(v, list):
            v2 = dst.setdefault(k, [])
            if isinstance(v2, list):
                v2 = v2 + v
            dst[k] = v2

        else:
            dst[k] = v
    return dst

class KubernetesPodMixins:
    '''A class of helpful methods for building pods.'''

    def _build_container_obj(self):
        container = {
            'name': 'brloz-job',
            'image': self.image,
            'imagePullPolicy': self.imagePullPolicy,
            'args': self.args,
            'env': self.env,
        }
        if self.imagePullSecrets:
            container['imagePullSecrets'] = self.imagePullSecrets
        if self.command:
            container['command'] = self.command
        if self.args:
            container['args'] = self.args

        return container

class KubernetesJob(KubernetesPodMixins, JobBase):

    def __init__(self, **kwargs):
        '''
        namespace: The Kubernetes namespace to operate in.
        name: The name of the job. The container used will have a derived name.
        image: The container image to use.
        imagePullSecrets: A list of secrets to use to pull the image. See Kubernetes documentation for this.
        imagePullPolicy: IfPresent, Always, or another Kubernetes supported policy.
        command: The command to run. A list of strings.
        args: Arguments to append to the command. A list of strings.
        body: A python object that is merged with the file API request before sending.
        '''
        JobBase.__init__(self, **kwargs)
        self.namespace = kwargs.get('namespace', 'default')
        self.name = kwargs.get('name', "brloz-"+str(uuid.uuid4()))
        self.image = kwargs.get('image', 'fedora:latest')
        self.command = kwargs.get('command', None) # List of strings.
        self.args = kwargs.get('args', None)       # List of strings.

        self.imagePullPolicy = kwargs.get('imagePullPolicy', 'Always')

        self.env = kwargs.get('env', [])
        self.env.append({ 'name': 'BRLOZ', 'value': '1' })

        # List of hashes of pull secrets. Eg [{'name': 'foot'}]
        self.imagePullSecrets = kwargs.get('imagePullSecrets', [])

        self.body = kwargs.get('body', {})

    def cleanup(self):
        _load_config()
        batchv1 = client.BatchV1Api()
        body = client.V1DeleteOptions(propagation_policy='Foreground', grace_period_seconds=0)
        res = batchv1.delete_namespaced_job(self.name, self.namespace, body = body)
        return res
    

    def status(self):
        _load_config()
        batchv1 = client.BatchV1Api()
        res = batchv1.list_namespaced_job(self.namespace, label_selector="brloz-job")
        for s in res.items:
            if self.name == s.metadata.name:
                return s.status
        return None

    
    def logs(self):
        _load_config()
        v1 = client.CoreV1Api()
        pods = v1.list_namespaced_pod(self.namespace, label_selector="job-name={}".format(self.name))
        logs = []
        for pod in pods.items:
            logs.append(v1.read_namespaced_pod_log(pod.metadata.name, self.namespace))

        return "\n".join(logs)

    def run(self):
        _load_config()
        batchv1 = client.BatchV1Api()

        v1job = {
            'apiVersion': 'batch/v1',
            'kind': 'Job',
            'metadata': {
                # Unique key of the Job instance
                'name': self.name,
                'labels': {
                    'brloz-job': 'brloz-job',
                }
            },
            'spec': {
                'template': {
                    'metadata': {
                        # 'name': self.name,
                        'labels': {
                            'brloz-job': 'brloz-job',
                            'job-name': self.name,
                            }
                        },
                    'spec': {
                        'imagePullSecrets': self.imagePullSecrets,
                        'containers':
                            [
                                self._build_container_obj(),
                                ],
                        # Do not restart containers after they exit.
                        'restartPolicy': 'Never',
                    }
                }
            }
            }
        
        v1job = _merge(v1job, self.body)
        resp = batchv1.create_namespaced_job(self.namespace, v1job)
        return resp
    
    def list_jobs(self):
        _load_config()
        v1 = client.CoreV1Api()

        print("Listing pods with their IPs:")

        if self.namespace:
            ret = v1.list_namespaced_pod(self.namespace, watch=False)
        else:
            ret = v1.list_pod_for_all_namespaces(watch=False)

        for i in ret.items:
            print("%s\t%s\t%s" % (i.status.pod_ip, i.metadata.namespace, i.metadata.name))


class KubernetesDeploymentJob(KubernetesPodMixins, JobBase):
    '''
    Deploy a container to Kubernetes. Clean up will remove it.
    '''

    def __init__(self, **kwargs):
        '''
        namespace: The Kubernetes namespace to operate in.
        name: The name of the job. The container used will have a derived name.
        image: The container image to use.
        imagePullSecrets: A list of secrets to use to pull the image. See Kubernetes documentation for this.
        imagePullPolicy: IfPresent, Always, or another Kubernetes supported policy.
        command: The command to run. A list of strings.
        args: Arguments to append to the command. A list of strings.
        body: A python object that is merged with the file API request before sending.
        '''
        JobBase.__init__(self, **kwargs)
        self.namespace = kwargs.get('namespace', 'default')
        self.name = kwargs.get('name', "brloz-"+str(uuid.uuid4()))
        self.image = kwargs.get('image', 'fedora:latest')
        self.command = kwargs.get('command', None) # List of strings.
        self.args = kwargs.get('args', None)       # List of strings.

        self.imagePullPolicy = kwargs.get('imagePullPolicy', 'Always')

        self.env = kwargs.get('env', [])
        self.env.append({ 'name': 'BRLOZ', 'value': '1' })

        # List of hashes of pull secrets. Eg [{'name': 'foot'}]
        self.imagePullSecrets = kwargs.get('imagePullSecrets', [])

        self.succeeded = None
        self.failed = None
        self.body = kwargs.get('body', {})
    
    def run(self):
        _load_config()
        v1apps = client.AppsV1Api()
        deployment = {
            'apiVersion': 'apps/v1',
            'kind': 'Deployment',
            'metadata': {
                # Unique key of the Job instance
                'name': self.name,
                'labels': {
                    'brloz-job': 'brloz-job',
                    'deployment-name': self.name,
                }
            },
            'spec': {
                'replicas': 1,
                'selector': {
                    'matchLabels': {
                        'deployment-name': self.name,
                    }
                },
                'template': {
                    'metadata': {
                        # 'name': self.name,
                        'labels': {
                            'brloz-job': 'brloz-job',
                            'deployment-name': self.name,
                            }
                        },
                    'spec': {
                        'imagePullSecrets': self.imagePullSecrets,
                        'containers':
                            [
                                self._build_container_obj(),
                                ],
                    }
                }
            }
        }

        deployment = _merge(deployment, self.body)
        v1apps.create_namespaced_deployment(self.namespace, deployment)

    def logs(self):
        return ''
    
    def status(self):
        _load_config()
        v1apps = client.AppsV1Api()
        resp = v1apps.list_namespaced_deployment(self.namespace, label_selector="deployment-name="+self.name)
        for status in resp.items:
            if not status.status.unavailable_replicas or status.status.unavailable_replicas == 0:
                self.succeeded = True
                self.failed = False
        return self

    def cleanup(self):
        _load_config()
        v1apps = client.AppsV1Api()
        body = client.V1DeleteOptions(propagation_policy='Foreground', grace_period_seconds=0)
        v1apps.delete_namespaced_deployment(self.name, self.namespace, body=body)

class KubernetesServiceJob(JobBase):
    '''
    Deploy a container to Kubernetes. Clean up will remove it.
    '''

    def __init__(self, **kwargs):
        '''
        namespace: The Kubernetes namespace to operate in.
        name: The name of the job. The container used will have a derived name.
        body: A body object to merge with the API call.
        '''
        JobBase.__init__(self, **kwargs)
        self.namespace = kwargs.get('namespace', 'default')
        self.name = kwargs.get('name', "brloz-"+str(uuid.uuid4()))
        self.type = kwargs.get('type', 'NodePort')
        self.selector = kwargs.get('selector', {
            'brloz-job': 'brloz-job'
        })
        self.ports = kwargs.get('ports', [{
                'name': 'http',
                'port': 80,
                'targetPort': 80,
            }])
        self.succeeded = None
        self.failed = None
        self.body = kwargs.get('body', {})
    
    def logs(self):
        return ''

    def status(self):
        _load_config()
        v1 = client.CoreV1Api()
        resp = v1.list_namespaced_service(self.namespace, label_selector="service-name="+self.name)
        for status in resp.items:
            self.succeeded = True
            self.failed = False

        return self

    def run(self):
        _load_config()
        v1 = client.CoreV1Api()
        service = {
            'apiVersion': 'v1',
            'kind': 'Service',
            'metadata': {
                'name': self.name,
                'labels': {
                    'service-name': self.name,
                    'brloz-job': 'brloz-job',
                }
            },
            'spec': {
                'ports': self.ports,
                'selector': self.selector,
            },
            'type': self.type
        }
        service = _merge(service, self.body)
        resp = v1.create_namespaced_service(self.namespace, service)
        return resp
    
    def cleanup(self):
        _load_config()
        v1 = client.CoreV1Api()
        resp = v1.delete_namespaced_service(self.name, self.namespace)
        return resp

class KubernetesConfigMapJob(JobBase):
    def __init__(self, **kwargs):
        '''
        namespace: The Kubernetes namespace to operate in.
        name: The name of the job. The container used will have a derived name.
        body: A body object to merge with the API call.
        '''
        JobBase.__init__(self, **kwargs)
        self.namespace = kwargs.get('namespace', 'default')
        self.name = kwargs.get('name', "brloz-"+str(uuid.uuid4()))
        self.succeeded = None
        self.failed = None
        self.data = kwargs.get('data', {
            'help.txt': '''You must specify a data value''',
        })
        self.body = kwargs.get('body', {})

    def run(self):
        _load_config()
        v1 = client.CoreV1Api()
        configmap = {
            'apiVersion': 'v1',
            'kind': 'ConfigMap',
            'metadata': {
                'name': self.name,
                'labels': {
                    'brloz-job': 'brloz-job',
                    'configmap-name': self.name,
                }
            },
            'data': self.data,
        }
        configmap = _merge(configmap, self.body)
        try:
            resp = v1.create_namespaced_config_map(self.namespace, configmap)
            return resp
        except Exception as exc:
            self.logdata = '{}: {}'.format(type(exc).__name__, exc)
            self.failed = '{}: {}'.format(type(exc).__name__, exc)
    
    def logs(self):
        return ''

    def cleanup(self):
        _load_config()
        v1 = client.CoreV1Api()
        resp = v1.delete_namespaced_config_map(self.name, self.namespace)
        return resp

    def status(self):
        _load_config()
        v1 = client.CoreV1Api()
        resp = v1.list_namespaced_config_map(self.namespace, label_selector='configmap-name='+self.name)
        for s in resp.items:
            self.succeeded = True
            self.failed = False

        return self


JobRegistry.register("kubernetes", KubernetesJob)
JobRegistry.register("kubernetesDeployment", KubernetesDeploymentJob)
JobRegistry.register("kubernetesService", KubernetesServiceJob)
JobRegistry.register("kubernetesConfigMap", KubernetesConfigMapJob)
