
from .filesystem import FileSystem

def from_config(config):
    if config.has_section('storage'):
        driver = config.get('storage', 'driver', fallback=None)
        if driver == 'filesystem':
            return FileSystem(config)
        else:
            raise Exception("No driver for type "+driver)
    else:
        raise Exception("No storage section.")
