import os

class FileSystem:
    '''
    This maps keybased storage into the filesystem.
    
    The / characters is interpreted as a path separator. Paths are created.
    
    This filesystem storage class treats files close to key-value entities.
    
    This choice is observable in the implementation of the list() method
    where 1) files are returned, not directories and 2) the files are listed as
    paths relative to the root of the FileSystem instance.

    Sematic use of this storage class should map well into implementations against 
    other types of blob storage.
    '''

    def __init__(self, config):
        '''
        config: A configparser.ConfigParser instance.
        '''
        if config.has_section('storage'):
            self.root = config.get('storage', 'path', fallback='./db')
            self.history = config.getint('storage', 'history', fallback=10)
        else:
            self.root = './db'
            self.history = 10
        self.root = os.path.abspath(self.root)
        os.makedirs(self.root, exist_ok=True)
    
    def path(self, file):
        '''Build a path to the file in the filesystem store.
        If the file would reside outside the store (.. is used to escape the store)
        then None is returned.
        '''
        p = os.path.abspath(os.path.join(self.root, file))
        if p.startswith(self.root):
            return p
        else:
            return None

    def write(self, f, data):
        '''Write a file.'''
        p = self.path(f)
        if p:
            os.makedirs(os.path.dirname(p), exist_ok=True)
            with open(p, 'w') as ostream:
                ostream.write(data)

    def read(self, f):
        '''
        Fully read a file.
        '''
        p = self.path(f)
        if p:
            with open(p, 'r') as istream:
                return istream.read()
    
    def exists(self, f):
        '''Return true if a file is in our storage directory and exists.'''
        p = self.path(f)
        return p and os.path.exists(p)

    def list(self, f):
        '''List all files under a directory.'''
        p = self.path(f)
        if p:
            striplen = len(self.root)+1
            for dirpath, dirnames, filenames, dirfd in os.fwalk(top=p, topdown=False, onerror=None):
                for fn in filenames:
                    yield(os.path.join(dirpath, fn)[striplen:])


    def delete(self, f):
        '''Delete all keys under this directory. If f is not a directory, then it is deleted.'''
        p = self.path(f)
        if p and os.path.exists(p):
            if os.path.isdir(p):
                # Delete tree
                for dirpath, dirnames, filenames, dirfd in os.fwalk(top=p, topdown=False, onerror=None):
                    for f in filenames:
                        os.unlink(f, dir_fd=dirfd)
                    for d in dirnames:
                        os.rmdir(d, dir_fd=dirfd)
                # Finally, remove the directory we've just walked and cleared.
                os.rmdir(p)

            else:
                # Delete a single file.
                os.unlink(f)
    