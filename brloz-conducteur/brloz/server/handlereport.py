
import json
import datetime

class ReportMixin:
    def listmetas(self, prefix):
        return sorted([ x for x in self.storage.list(prefix) if x.endswith('/meta.json') ])

class HandlePostReport(ReportMixin):

    def __init__(self, storage, history):
        self.storage = storage
        self.history = history

    def prunehistory(self, prefix):
        '''Using the given prefix, list all files and keep the most recent subtrees.
        '''
        lst = self.listmetas(prefix)
        while len(lst) > self.history:
            # Strip off old /meta.json.
            oldTree = lst[0][:-10]
            self.storage.delete(oldTree)
            lst = lst[1:]
        
    def on_post(self, req, resp, reportName, passfail='pass'):
        attempt = 0
        timestamp = datetime.datetime.utcnow().strftime('%Y%m%dT%H%M%S')
        prefix = '{}/{}-{}'.format(reportName, timestamp, attempt)
        metaname = prefix + '/meta.json'

        # This loop avoids collisions, but not necessarily races.
        while self.storage.exists(metaname):
            attempt += 1
            prefix = '{}/{}-{}'.format(reportName, timestamp, attempt)
            metaname = prefix + '/meta.json'

        dataname = prefix + '/data'

        self.storage.write(metaname, json.dumps({
            'timestamp': timestamp,
            'pass': passfail.lower() in ("yes", "pass", "passed", "true", "t", "1"),
            'name': reportName,
        }, ensure_ascii=False))

        self.storage.write(dataname, str(req.stream.read(), 'utf-8'))

        self.prunehistory(reportName)

        entity = {'status': 'OK', 'meta': metaname, 'data': dataname, 'timestamp': timestamp}
        resp.body = json.dumps(entity, ensure_ascii=False)
        

class HandleGetReport(ReportMixin):
    def __init__(self, storage, history):
        self.storage = storage
        self.history = history

    def on_get(self, req, resp, **kwargs):
        if 'filename' in kwargs:
            resp.content_type = 'text/plain'
            resp.body = self.storage.read('/'.join([kwargs['reportName'], kwargs['timestamp'], kwargs['filename']]))

        elif 'timestamp' in kwargs:
            lst = list(self.storage.list('/'.join([kwargs['reportName'], kwargs['timestamp']])))
            resp.body = json.dumps(lst)

        elif 'reportName' in kwargs:
            lst = [ '/'.join(x.split('/')[0:2]) for x in self.listmetas(kwargs['reportName']) ]
            resp.body = json.dumps(lst)

        else:
            lst = list(dict([ (x.split('/')[0], 1) for x in self.listmetas('') ]).keys())
            resp.body = json.dumps(lst)

