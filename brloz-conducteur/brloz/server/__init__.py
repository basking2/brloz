'''
Brloz can be run as a single command line tool or as a server
to which pipelines can be posted and a history of results collected.

The server only keeps track of pass/fail statistics. The user my interrogate
the job logs to decide what course of action is needed.
'''

import falcon
import multiprocessing
from gunicorn.app.base import BaseApplication
import brloz
import brloz.storage
import json

from .handlereport import HandleGetReport
from .handlereport import HandlePostReport
from .handlepipeline import HandlePipeline

app = falcon.API()

class StandaloneApplication(BaseApplication):

    def __init__(self, app, options=None):
        self.options = options or {}

        if not 'workers' in self.options:
            self.options['workers'] =  (multiprocessing.cpu_count() * 2) + 1

        self.application = app

        super().__init__()

    def load_config(self):
        config = {key: value for key, value in self.options.items()
                  if key in self.cfg.settings and value is not None}
        for key, value in config.items():
            self.cfg.set(key.lower(), value)

    def load(self):
        return self.application


def ensure_storage_section(config):
    '''Make sure there is a storage configuration, even if it is a default.'''
    if not config.has_section('storage'):
        config.add_section('storage')
        config.set('storage', 'driver', 'filesystem')
        config.set('storage', 'path', './db')
        config.set('storage', 'history', '10')

def default404(req, resp):
    raise falcon.HTTPNotFound(
            title='The resource was not found.',
            description='''
            Supported paths are /submit/report and /report.
            ''')

class IndexResponder:
    def on_get(self, req, resp):
        resp.content_type = 'text/html'
        resp.body = '''
        <!DOCTYPE html>
        <html lang="en">
          <head>
            <meta charset="utf-8">
            <title>Brloz</title>
          </head>
          <body>
            <a href="report">report</a>
          </body>
        </html>
        '''


def gunicorn(config):

    ensure_storage_section(config)

    storage = brloz.storage.from_config(config)
    options = {
        'bind': '{}:{}'.format(
            config.get('server', 'bind', fallback='0.0.0.0'),
            config.get('server', 'port', fallback=8000),
            ),
    }

    if config.has_option('server', 'workers'):
        options['workers'] = config.getint('server', 'workers')
    
    if config.has_section('gunicorn'):
        for key, value in config.items('gunicorn'):
            options[key] = value

    handleGetReport = HandleGetReport(
        storage,
        config.getint('storage', 'history', fallback=4))
    handlePostReport = HandlePostReport(
        storage,
        config.getint('storage', 'history', fallback=4))
    app.add_route('/submit/report/{reportName}/{passfail}', handlePostReport)

    app.add_route('/report', handleGetReport)
    app.add_route('/report/{reportName}', handleGetReport)
    app.add_route('/report/{reportName}/{timestamp}', handleGetReport)
    app.add_route('/report/{reportName}/{timestamp}/{filename}', handleGetReport)

    app.add_route('/pipeline', HandlePipeline())
    app.add_route('/', IndexResponder())

    app.add_sink(default404, '/')

    StandaloneApplication(app, options).run()


