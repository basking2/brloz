import brloz

import multiprocessing

class HandlePipeline:
    def __init__(self):
        self.process = None

    def on_post(self, req, resp):
        '''POST yaml.'''
        if req.content_type == 'application/x-yaml' or req.content_type == 'text/yaml':
            pipeline = brloz.pipeline.Pipeline.fromYaml(req.stream)

            if self.process and not self.process.is_alive():
                self.process.join()
                self.process = None

            if self.process:
                resp.content = 'text/plain'
                resp.body = "This worker already has a job running."
                resp.status = '529 Site is overloaded'
            else:

                def pipeline_run():
                    pipeline.run()

                self.process = multiprocessing.Process(target=pipeline_run)
                self.process.start()
                resp.content_type = 'text/plain'
                resp.body = "OK"
