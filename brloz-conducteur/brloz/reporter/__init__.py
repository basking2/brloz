'''
Reporters are objects that can take a result and report on it.
'''

class ReporterRegistryImpl:
    def __init__(self):
        self.reporters = {}
    def reporter(self, name, **kwargs):
        return self.reporters[name](**kwargs)
    def register(self, name, clazz):
        self.reporters[name] = clazz

ReporterRegistry = ReporterRegistryImpl()

class ReporterBase:
    def __init__(self):
        pass

class StdOutReporter(ReporterBase):
    def __init__(self, **kwargs):
        ReporterBase.__init__(self)
        self.verbose = kwargs.get('verbose', False)

    def report(self, name, status, results):
        if status.succeeded:
            if results.succeeded:
                print(name, "succeeded.")
            else:
                print(name, "failed in collection.")
        else:
            print(name, "failed in job run.")

        if self.verbose:
            print(results.raw)

import http.client

class HttpPostReporter(ReporterBase):
    def __init__(self, **kwargs):
        '''
        port: A number of the port to connect to.
        verb: HTTP verb.
        template: User specified template. Currently, if this is specified,
                  the {content} is expanded to the report content.
        format: This is currently only set to "simple". 
                Future work will allow more complex and complete formats
        host: The hostname to connect to.
        usessl: Should we use SSL / TLS.
        path: The path to post to. By default this is /submit/report/{name}/{passfail}
        ignorepass: Do not report passing results. Default off.
        ignorefail: Do not report failing results. Default off.

        Future work is to let this accept a URL.
        '''
        self.verb = kwargs.get('verb', 'POST')
        self.port = int(kwargs.get('port', 8000))
        self.host = kwargs.get('host', 'localhost')
        self.format = kwargs.get('format', 'simple')
        self.usessl = bool(kwargs.get('usessl', False))
        self.template = kwargs.get('template', None)
        self.headers = kwargs.get('headers', {})
        self.ignorepass = bool(kwargs.get('ignorepass', False))
        self.ignorefail = bool(kwargs.get('ignorefail', False))
        self.path = kwargs.get('path', '/submit/report/{name}/{passfail}')

    def report(self, name, status, results):
        passfail='pass'
        if status.succeeded:
            if results.succeeded:
                passfail = 'pass'
            else:
                passfail = 'fail'
        else:
            passfail = 'fail'

        if self.ignorepass and passfail == 'pass':
            return
        if self.ignorefail and passfail == 'fail':
            return

        path = self.path.format(name=name, passfail=passfail)

        if self.template:
            body = self.template.format(name=name, passfail=passfail, content=results.raw)
        else:
            body = results.raw

        if self.usessl:
            client = http.client.HTTPSConnection(self.host, self.port)
        else:
            client = http.client.HTTPConnection(self.host, self.port)
    
        try:
            client.request(self.verb, path, body, headers=self.headers)
            resp = client.getresponse()
            print(resp.status, resp.reason)
        finally:
            client.close()

ReporterRegistry.register("stdout", StdOutReporter)
ReporterRegistry.register('httpPost', HttpPostReporter)
ReporterRegistry.register("http", HttpPostReporter)
