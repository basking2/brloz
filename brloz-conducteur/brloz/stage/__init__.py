from .. import CollectorRegistry

class Stage:
    def __init__(self, name, timeout, job, collector=CollectorRegistry.collector('all'), reporters=[]):
        '''Constructor.

        Parameters
        ----------
        name : A name.
        timeout: How long, in seconds, to wait for completion.
        job : Job
            An object that matches the job interface. Specifically,
            run, wait, logs, and cleanup.
        collector : A results collector.
        reporters : []Reporter
            How a job result is reported. A typical reporter might
            be LocalReporter which reports to the local system state
            or it might be the SlackReporter to send results to Slack.

        '''
        self.name = name
        self.job = job
        self.timeout = timeout
        self.collector = collector
        self.reporters = reporters

    def run(self):
        self.job.run()
        status = self.job.wait(timeout=self.timeout)
        result = self.collector.collect(self.job.logs())
        for r in self.reporters:
            r.report(self.name, status, result)
    
    def cleanup(self):
        self.job.cleanup()
