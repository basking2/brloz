'''
Brloz
-----

Brloz orchestrates Stages. Stages are a single Job, a ResultsGatherer, and a list of Reporters.
The Job is run and the results passed to the gather. The gatherer produces a Result and that is 
passed to each of the Reporters.

'''

# The Job Registry
from .job import JobRegistry
from .collector import CollectorRegistry
from .reporter import ReporterRegistry
from .stage import Stage
from .pipeline import Pipeline
from .config import load_config