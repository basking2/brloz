import re

gtre = re.compile('\\s*(\\w+)\\s*>\\s*(\\d+\\.?\\d*)\\s*')
gtere = re.compile('\\s*(\\w+)\\s*>=\\s*(\\d+\\.?\\d*)\\s*')
ltre = re.compile('\\s*(\\w+)\\s*<\\s*(\\d+\\.?\\d*)\\s*')
ltere = re.compile('\\s*(\\w+)\\s*<=\\s*(\\d+\\.?\\d*)\\s*')
eqre = re.compile('\\s*(\\w+)\\s*==\\s*(\\d+\\.?\\d*)\\s*')

streqre = re.compile('\\s*(\\w+)\\s*==\\s*(.*)$')
nere = re.compile('\\s*(\\w+)\\s*!=\\s*(\\d+\\.?\\d*)\\s*')
rere = re.compile('\\s*(\\w+)\\s*~\\s*([^\\s].*)$')

def build(s):
    '''
    Build a lambda that takes a hash, looks up a key, and compares the result with the given check.
    '''
    m = gtre.match(s)
    if m:
        key = m[1]
        val = float(m[2])
        return lambda hash: hash[key] > val

    m = gtere.match(s)
    if m:
        key = m[1]
        val = float(m[2])
        return lambda hash: hash[key] >= val

    m = ltre.match(s)
    if m:
        key = m[1]
        val = float(m[2])
        return lambda hash: hash[key] < val

    m = ltere.match(s)
    if m:
        key = m[1]
        val = float(m[2])
        return lambda hash: hash[key] <= val

    m = eqre.match(s)
    if m:
        key = m[1]
        val = float(m[2])
        return lambda hash: hash[key] == val

    m = streqre.match(s)
    if m:
        key = m[1]
        val = m[2]
        return lambda hash: hash[key] == val

    m = rere.match(s)
    if m:
        key = m[1]
        val = re.compile(m[2])
        return lambda hash: val.search(hash[key])

    raise Exception("FAILED TO FIND FOR "+s)

