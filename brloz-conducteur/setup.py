#!/usr/bin/env python3
import os

datafiles = []
for (dirpath, dirnames, filenames) in os.walk("resources"):
    for filename in filenames:
        datafiles.append(os.path.join(dirpath, filename))

version = '0.0.1'
if 'CI_COMMIT_TAG' in os.environ:
    # Get the version and remove the first character, a 'v'.
    version = os.environ['CI_COMMIT_TAG'][1:]

from setuptools import setup, find_packages

setup(
    name='brloz-conducteur',
    version=version,
    packages=find_packages(),
    scripts=[
        'bin/brloz',
    ],
    data_files=[('', datafiles)],
    install_requires=[
        'gunicorn',
        'falcon',
        'kubernetes',
    ],
    license='MIT',
)

