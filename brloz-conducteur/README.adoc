= Brloz Conducteur

== Development

----
pipenv --python 3
pipenv install --dev -e .
----

== Running the Server

=== Directly

----
brloz --bind 0.0.0.0 --port 8080 -s
----

=== Docker Images

----
# Simple.
docker -p 8000:8000 brloz

# More complext.
docker -v local_db:/brloz/db -v brlozrc:/etc/brlozrc -p 8000:8000 --name=brloz brloz
----

=== Server API

[horizontal]
`GET /report`:: Return all reports.
`GET /report/{name}`:: Return all timestamps.
`GET /report/{name}/{timestamp}`:: Return all files in a report.
`GET /report/{name}/{timestamp}/meta.json`:: Return a report JSON object.
`GET /report/{name}/{timestamp}/data`:: Return a report log file.
`POST /submit/report/{name}/<pass|fail>`:: Report the success or failure of a pipeline.
`POST /pipeline`:: Post a YAML pipeline. Note the `Content-Type` must be `appliction/x-yaml`
    or `text/yaml`. Consider using the LocalHttp reporter to collect
    report results to `/reports/{name}`.

=== Pipelines

This is in a separate link:PIPELINE.adoc[pipeline] document.