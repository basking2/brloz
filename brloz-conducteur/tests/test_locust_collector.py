import pytest

from brloz.collector import CollectorRegistry
import os

def test_locust_collector_01():
    log = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'locust_log_result.txt')
    with open(log, 'rt') as f:
        log = f.read()
        collector = CollectorRegistry.collector('locust', checks=[
            {
                'if': ['type == statistics', 'id ~ ^.*$'],
                'then': ['average > 10', 'average < 10'],
                'name': 'Contradiction check.',
                }
            ])
        result = collector.collect(log)
    
    assert not result.succeeded
    assert result.message == 'Failure for Failed check. on record  GET /a/cF[x=13]                         1     0(0.00%)  |      11      11      11      11  |    0.00    0.00'

def test_locust_collector_02():
    log = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'locust_log_result.txt')
    with open(log, 'rt') as f:
        log = f.read()

        # GET /a/rF[x=9]                       10376     0(0.00%)  |       9       6     314       8  |   34.59    0.00
        collector = CollectorRegistry.collector('locust', checks=[
            {
                'if': [
                    'type == statistics',
                    'id ~ GET /a/rF\\[x=9\\]'
                    ],
                'then': [
                    'requests == 10376',
                    'failure_percentage == 0.0',
                    'failures == 0.0',
                    'average == 9',
                    'minimum == 6',
                    'maximum == 314',
                    'median == 8',
                    'requests_per_second > 0',
                    'failures_per_second <= 10',
                    ],
                'name': 'Test 02'
                }
            ])
        result = collector.collect(log)
    
    assert result.succeeded

def test_locust_collector_03():
    log = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'locust_log_result.txt')
    with open(log, 'rt') as f:
        log = f.read()

        # Aggregated                                                    249591     0(0.00%)  |      11       1     883       9  |  831.97    0.00

        collector = CollectorRegistry.collector('locust', checks=[
            {
                'if': [
                    'type == statistics',
                    'id == Aggregated',
                    ],
                'then': [
                    'requests > 10376',
                    'failure_percentage < 1.0',
                    'average <= 50',
                    'minimum <= 50',
                    'maximum <= 1000',
                    'median <= 50',
                    'requests_per_second > 0',
                    'failures_per_second <= 10',
                    ],
                'name': 'Test 03'
                }
            ])
        result = collector.collect(log)
    
    assert result.succeeded
