import os

import brloz

def test_pipeline_yaml():
    yaml = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'test-pipeline-01.yaml')
    with open(yaml, 'r') as f:
        pipeline = brloz.Pipeline.fromYaml(f)
    
    pipelineReporter = brloz.ReporterRegistry.reporter('pipeline')
    for s in pipeline.stages:
        s.reporters.append(pipelineReporter)
    
    pipeline.run()

    assert pipelineReporter.successCount == 1
    assert pipelineReporter.failureCount == 1
    assert pipelineReporter.stageLog[0]['stage'] == 'Stage 1'
    assert pipelineReporter.stageLog[0]['status'] == 'passed'
    assert pipelineReporter.stageLog[0]['when'] == 'results'

    assert pipelineReporter.stageLog[1]['stage'] == 'Stage 2'
    assert pipelineReporter.stageLog[1]['status'] == 'failed'
    assert pipelineReporter.stageLog[1]['when'] == 'job'