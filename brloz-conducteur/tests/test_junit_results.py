import pytest

from brloz.collector import CollectorRegistry
import os

def test_parse_junit():
    junitxml = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'junit.xml')
    with open(junitxml, 'rt') as f:
        xml = f.read()
        collector = CollectorRegistry.collector('junit')
        result = collector.collect(xml)
    
    assert not result.succeeded
    assert result.message == 'Failed because of 1 errors and 1 failures from 4 tests. 1 skipped.'