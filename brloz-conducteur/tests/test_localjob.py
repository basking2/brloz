import pytest

from brloz.job import LocalJob

def test_parse_local_pass():
    job = LocalJob(command="ls", args=".")
    job.run()
    job.wait(60)
    l = job.logs().split("\n")
    assert 'Pipfile' in l
    assert job.status().succeeded
    assert not job.status().failed
    job.cleanup()

def test_parse_local_fail():
    job = LocalJob(command=["ls", "./does_not_exist" ], args=".")
    job.run()
    job.wait(60)
    l = job.logs().split("\n")
    assert not job.status().succeeded
    assert job.status().failed
    job.cleanup()