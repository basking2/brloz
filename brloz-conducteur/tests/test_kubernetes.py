# Tests that only run if the environment RUN_KUBERNETES is set.

import os
import brloz 
import pytest

RUN = os.environ.get('RUN_KUBERNETES', None)

if RUN:

    @pytest.mark.skip(reason="Use only when needed.")
    def test_kubernetes_job():
        job = brloz.JobRegistry.job("kubernetes", imagePullPolicy="IfNotPresent", command=["echo", "OK!"])
        try:
            s = job.run()
            s = job.wait(timeout=600)
        except Exception as e:
            print(e)
        finally:
            job.cleanup()

    @pytest.mark.skip(reason="Use only when needed.")
    def test_kubernetes_deployment_job():
        deployment = brloz.JobRegistry.job("kubernetesDeployment", image="nginx:latest", imagePullPolicy="IfNotPresent")
        try:
            deployment.run()
            result = deployment.wait(timeout=30)
            print(result)
        finally:
            deployment.cleanup()

    @pytest.mark.skip(reason="Use only when needed.")
    def test_kubernetes_service_job():
        service = brloz.JobRegistry.job("kubernetesService", name="brloz-service-test")
        try:
            service.run()
            result = service.wait(timeout=30)
            print(result)
        finally:
            service.cleanup()

    @pytest.mark.skip(reason="Use only when needed.")
    def test_kubernetes_configmap():
        service = brloz.JobRegistry.job("kubernetesConfigMap", name="brloz-configmap-test", data={'foo': 'Foo data', 'bar': 'Bar data'})
        try:
            service.run()
            result = service.wait(timeout=30)
            print(result)
        finally:
            service.cleanup()
    
