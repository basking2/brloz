import pytest

import brloz
import http.server
import threading

def test_reporter_http():
    stage = brloz.Stage(
        "testStage",
        10,
        job=brloz.JobRegistry.job("local", command="ls"),
        collector=brloz.CollectorRegistry.collector("regex", regex='.*'),
        reporters=[
            brloz.ReporterRegistry.reporter('httpPost', host='localhost', port=18080),
            brloz.ReporterRegistry.reporter('stdout'),
        ]
    )

    body = []
    path = []

    class MyHandler(http.server.BaseHTTPRequestHandler):
        def __init__(self, request, client_address, server):
            http.server.BaseHTTPRequestHandler.__init__(self, request, client_address, server)

        def do_POST(self):
            self.send_response(200)
            self.send_header("Content-type", "text/plain")
            self.end_headers()
            body.append(self.rfile.read())
            path.append(self.path)
            self.wfile.write(bytes('OK', 'utf-8'))

    class MyThread(threading.Thread):
        def __init__(self, **kwargs):
            threading.Thread.__init__(self, **kwargs)
            self.server = http.server.HTTPServer(('127.0.0.1', 18080), MyHandler)
        def run(self):
            self.server.handle_request()

        def server_close(self):
            self.server.server_close()

    thread = MyThread()
    thread.start()
    pipeline = brloz.Pipeline('test-http', [stage])
    pipeline.run()
    # Exec back into another python script to abort the test harness.
    thread.server_close()
    thread.join()
    assert path[0] == '/submit/report/testStage/pass'

