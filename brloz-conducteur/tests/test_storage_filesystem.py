import pytest

from configparser import ConfigParser
from brloz.storage import FileSystem

def test_storage_filesystem_emptyconfig():
    config = ConfigParser()
    fs = FileSystem(config)

def test_storage_filesystem_basic():
    config = ConfigParser()
    config.add_section('storage')
    config.set('storage', 'history', '2')
    config.set('storage', 'path', './test_db')
    fs = FileSystem(config)

def test_storage_filesystem_delete():
    config = ConfigParser()
    config.add_section('storage')
    config.set('storage', 'history', '2')
    config.set('storage', 'path', './test_db')
    fs = FileSystem(config)
    fs.write('a/b/c/d1', 'Hi!')
    fs.write('a/b/c/d2', 'Hi!')
    fs.delete('')

def test_storage_filesystem_list():
    config = ConfigParser()
    config.add_section('storage')
    config.set('storage', 'history', '2')
    config.set('storage', 'path', './test_db')
    fs = FileSystem(config)
    fs.write('a/b/c/d1', 'Hi!')
    fs.write('a/b/c/d2', 'Hi!')

    files = []
    for f in fs.list('a/b'):
        files.append(f)

    files = sorted(files)

    assert files[0] == 'a/b/c/d1'
    assert files[1] == 'a/b/c/d2'

    for i in files:
        assert fs.exists(i)
        d = fs.read(i)
        assert d == 'Hi!'

    fs.delete('')
