import pytest

from brloz.job import JobRegistry
from brloz.reporter import ReporterRegistry
from brloz.collector import CollectorRegistry
from brloz.stage import Stage

def test_stage():
    class MyReporter:
        def __init__(self):
            self.reports = 0
        def report(self, name, status, result):
            assert name == 'testStage'
            assert status.succeeded
            assert not status.failed

            assert result.succeeded
            assert not result.failed
            assert result.raw
            assert result.message
            self.reports += 1

    myReporter = MyReporter()
    stage = Stage(
        "testStage",
        10,
        job=JobRegistry.job("local", command="ls"),
        collector=CollectorRegistry.collector("regex", regex='.*'),
        reporters=[
            ReporterRegistry.reporter('stdout'),
            myReporter
        ]
    )

    stage.run()
    stage.cleanup()
    assert myReporter.reports == 1

