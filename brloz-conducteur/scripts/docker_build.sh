#!/bin/sh

set -xe

# Go to the directory above this script's.
cd "`dirname $0`" && cd ..

docker build --build-arg CI_COMMIT_TAG="${CI_COMMIT_TAG:-0.0.1}" -t "basking2/brloz-conducteur:latest" .

if [ -n "$CI_COMMIT_TAG" ] && [ -n "$DOCKERHUB_API_KEY" ]; then
	# set -e above will exit if this fails.
	echo $CI_COMMIT_TAG | grep -E '^v\d+\.\d+\.\d+$' > /dev/null

	VERSION=`echo $CI_COMMIT_TAG | sed -E 's/^v(.*)$/\1/'`

	docker login --password "${DOCKERHUB_API_KEY}" -u basking2
	docker tag "basking2/brloz-conducteur:latest" "basking2/brloz-conducteur:${VERSION}"
	docker push "basking2/brloz-conducteur:${VERSION}"
	docker push "basking2/brloz-conducteur:latest"
fi

